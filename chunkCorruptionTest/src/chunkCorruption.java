/**
 * Created by iyro on 9/23/15.
 */

import chunk.chunk;

import java.io.*;
import java.util.Random;

public class chunkCorruption {
    protected chunk chunk;
    String chunkName;

    public chunkCorruption(String chunkName) {
        this.chunkName = chunkName;
        FileInputStream chunkPath = null;
        try {
            chunkPath = new FileInputStream("/tmp/" + chunkName);

            ObjectInputStream fin = new ObjectInputStream(chunkPath);

            this.chunk = (chunk) fin.readObject();
            chunkPath.close();
            chunkPath.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]) {
        chunkCorruption c = new chunkCorruption(args[0]);

        Random rn = new Random();
        c.getChunk().getData()[rn.nextInt(c.getChunk().getData().length)] = '\0';
        c.getChunk().getData()[rn.nextInt(c.getChunk().getData().length)] = '\0';
        c.getChunk().getData()[rn.nextInt(c.getChunk().getData().length)] = '\0';

        c.writeChunk();
    }

    public void writeChunk() {
        FileOutputStream chunkPath = null;
        try {
            chunkPath = new FileOutputStream("/tmp/" + chunkName);

            ObjectOutputStream fout = new ObjectOutputStream(chunkPath);

            fout.writeObject(chunk);
            chunkPath.close();
            chunkPath.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public chunk getChunk() {
        return chunk;
    }
}
