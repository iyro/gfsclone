/**
 * Created by iyro on 8/29/15.
 */

import chunk.*;
import chunkServerInfo.hostAddress;
import controllerRequests.chunkInfoRequest;
import controllerRequests.chunkInfoResponse;
import hearbeat.majorHeartbeat;
import hearbeat.minorHeartbeat;
import packets.packetStructure;

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static controllerRequests.chunkInfoRequest.FILE_READ;
import static packets.packetStructure.*;

public class chunkServer {
    protected static int chunkServerPort = 0;
    protected static Vector<String> chunkServerIP = new Vector<String>();
    protected static ServerSocket chunkServerSocket = null;

    protected static String controllerIP = "localhost";
    protected static int controllerPort = 43210;
    protected static Socket controllerSocket;

    private static ConcurrentHashMap<String, chunkMetadata> localChunkList = null;
    private static ConcurrentHashMap<String, chunkMetadata> localChunkListNew = null;

    public static Vector<hostAddress> getStrippedHostList(hostAddress h, Vector<hostAddress> v) {
        Vector<hostAddress> strippedList = new Vector<>(v);

        for (Iterator<hostAddress> iterator = strippedList.iterator(); iterator.hasNext(); ) {
            hostAddress r = iterator.next();
            if (h.getHostIp().equalsIgnoreCase(r.getHostIp()) && h.getHostPort() == r.getHostPort()) {
                // Remove the current element from the iterator and the list.
                iterator.remove();
            }
        }

        return strippedList;
    }

    public static void main(String args[]) {

        if (args.length != 2) {
            System.out.println("-- <FATAL ERROR> Invalid or no arguments.");
            System.out.println("Usage : java -jar ReplicatedChunkServer <Controller IP> <Controller Port>");
            System.exit(1);
        }

        if (checkIPv4(args[0]))
            controllerIP = args[0];
        else {
            System.out.println("-- <FATAL ERROR> Invalid IP string " + args[0] + ".");
            System.out.println("Usage : java -jar ReplicatedChunkServer <Controller IP> <Controller Port>");
            System.exit(1);
        }

        try {
            controllerPort = Integer.parseInt(args[1]);
            if (controllerPort < 1024 || controllerPort > 65535) {
                throw new Exception();
            }
        } catch (Exception e) {
            System.out.println("-- <FATAL ERROR> Invalid Port " + args[1] + ".");
            System.out.println("Usage : java -jar ReplicatedChunkServer <Controller IP> <Controller Port>");
            System.exit(1);
        }

        try {
            chunkServerSocket = new ServerSocket(chunkServerPort);
        } catch (IOException e) {
            throw new RuntimeException("-- <ERROR> Cannot open port " + chunkServerPort, e);
        }

        chunkServerPort = chunkServerSocket.getLocalPort();

        System.setProperty("java.net.preferIPv4Stack", "true");
        Enumeration e = null;
        try {
            e = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e1) {
            e1.printStackTrace();
        }
        while (e.hasMoreElements()) {
            NetworkInterface n = (NetworkInterface) e.nextElement();
            Enumeration ee = n.getInetAddresses();
            while (ee.hasMoreElements()) {
                InetAddress i = (InetAddress) ee.nextElement();
                if (i instanceof Inet6Address || i.isLoopbackAddress() || i.isSiteLocalAddress())
                    continue;
                chunkServerIP.add(i.getHostAddress());
                System.out.println("-- <INFO> Chunk Server listening on " + i.getHostAddress() + ":" + chunkServerSocket.getLocalPort());
            }
        }

        try {
            controllerSocket = new Socket(controllerIP, controllerPort);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        System.out.println("-- <INFO> Connected to controller (" + controllerSocket.getRemoteSocketAddress() + ").");

        localChunkList = new ConcurrentHashMap<String, chunkMetadata>();
        localChunkListNew = new ConcurrentHashMap<String, chunkMetadata>();

        chunkServerHeartbeatThread heartbeatThread = new chunkServerHeartbeatThread();
        new Thread(heartbeatThread).start();

        while (true) {
            Socket clientSocket = null;
            try {
                clientSocket = chunkServerSocket.accept();
                new Thread(
                        new chunkServerWorkerThread(
                                clientSocket)
                ).start();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public static final boolean checkIPv4(final String ip) {
        boolean isIPv4;
        try {
            final InetAddress inet = InetAddress.getByName(ip);
            isIPv4 = inet.getHostAddress().equals(ip)
                    && inet instanceof Inet4Address;
        } catch (final UnknownHostException e) {
            isIPv4 = false;
        }
        return isIPv4;
    }

    static class chunkServerHeartbeatThread implements Runnable {
        public void run() {
            System.out.println("-- <INFO> Heartbeat Thread Started.");
            int heartbeatCounter = 0;
            packetStructure packet;
            minorHeartbeat minor;
            majorHeartbeat major;
            ObjectOutputStream out = null;
            File file = new File("/tmp");
            hostAddress addr = new hostAddress(chunkServerIP.firstElement(), chunkServerPort);

            try {
                out = new ObjectOutputStream(controllerSocket.getOutputStream());

                while (!controllerSocket.isClosed()) {
                    if (heartbeatCounter != 0) {

                        minor = new minorHeartbeat(addr, file.getFreeSpace(), localChunkListNew, localChunkList.keySet().size());

                        packet = new packetStructure(CHUNK_SERVER_MINOR_HEARTBEAT, minor);
                        System.out.println("<- <" + controllerSocket.getRemoteSocketAddress() + "> Minor Heartbeat.");
                        out.writeObject(packet);

                        localChunkListNew = new ConcurrentHashMap<String, chunkMetadata>();
                    } else {
                        major = new majorHeartbeat(addr, file.getFreeSpace(), localChunkList, localChunkList.keySet().size());

                        packet = new packetStructure(CHUNK_SERVER_MAJOR_HEARTBEAT, major);
                        System.out.println("<- <" + controllerSocket.getRemoteSocketAddress() + "> Major Heartbeat.");
                        out.writeObject(packet);
                        localChunkListNew = new ConcurrentHashMap<String, chunkMetadata>();
                    }

                    Thread.sleep(30000); //TODO: Update this to 30

                    heartbeatCounter++;
                    heartbeatCounter %= 10;
                }
            } catch (IOException e) {
                System.out.println("-- <FATAL ERROR> Controller Dead. Quitting.");
                System.exit(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    static class chunkServerWorkerThread implements Runnable {
        protected Socket clientSocket = null;

        public chunkServerWorkerThread(Socket clientSocket) {
            this.clientSocket = clientSocket;
        }

        public void run() {
            System.out.println("-- <INFO> New worker thread started for " + clientSocket.getRemoteSocketAddress());
            packetStructure packet = null;
            ObjectInputStream clientIS = null;
            ObjectOutputStream clientOS = null;

            chunk c = null;

            try {
                clientIS = new ObjectInputStream(clientSocket.getInputStream());
                clientOS = new ObjectOutputStream(clientSocket.getOutputStream());
                packet = (packetStructure) clientIS.readObject();
            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
            } catch (IOException e) {
                try {
                    System.out.println("-- <ERROR> Client Failed or Dead.");
                    clientSocket.close();
                } catch (IOException e1) {
                    e.printStackTrace();
                }
            }

            int packetType = packet.getPacketType();
            switch (packetType) {
                case CHUNK_SLICE_READ_REQUEST: {
                    chunkSliceRequest request = null;
                    FileInputStream file;
                    ObjectInputStream fin = null;

                    try {
                        request = new chunkSliceRequest(packet.getPayloadPacket());

                        System.out.print("-> <" + clientSocket.getRemoteSocketAddress() + "> Slice Read " + request.getChunkName() + "{ ");
                        for (Integer i : request.getSliceList()) {
                            System.out.print(i + " ");
                        }
                        System.out.println("}");

                        fin = new ObjectInputStream(new FileInputStream("/tmp/" + request.getChunkName()));
                        c = (chunk) fin.readObject();

                        HashMap<Integer, byte[]> slices = new HashMap<>();
                        int index = 0;
                        for (Integer i : request.getSliceList()) {
                            byte[] b;
                            System.out.println("Array length : " + c.getData().length);
                            System.out.println("Read :" + (c.getData().length - ((i - 1) * chunk.SLICE_SIZE)));
                            if (c.getData().length - ((i - 1) * chunk.SLICE_SIZE) > chunk.SLICE_SIZE) {
                                System.out.println("Not Last slice");
                                System.out.print("Offset : " + (i - 1) * chunk.SLICE_SIZE);
                                b = Arrays.copyOfRange(c.getData(), (i - 1) * chunk.SLICE_SIZE, i * chunk.SLICE_SIZE);
                            } else {
                                System.out.println("Last slice");
                                int offset = (i - 1) * chunk.SLICE_SIZE;
                                System.out.print("Offset : " + offset);
                                b = Arrays.copyOfRange(c.getData(), offset, (c.getData().length));
                            }

                            slices.put(i, b);
                        }

                        System.out.print("<- <" + clientSocket.getRemoteSocketAddress() + "> Slice Read Response " + slices.keySet() + "{");
                        for (Integer i : slices.keySet()) {
                            System.out.print(i + " ");
                        }
                        System.out.println("}");

                        clientOS.writeObject(new packetStructure(CHUNK_SLICES, new chunkSlices(slices)));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                break;
                case CHUNK_READ_REQUEST: {
                    chunkRequest clientRequest = null;
                    FileInputStream file;
                    ObjectInputStream fin = null;
                    packetStructure response;


                    try {
                        clientRequest = new chunkRequest(packet.getPayloadPacket());
                        System.out.println("-> <" + clientSocket.getRemoteSocketAddress() + "> Read " + clientRequest.getChunkName() + ".");
                    } catch (IOException e) {
                        System.out.println("-- <ERROR> Unresolved Payload.");
                        break;
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                        break;
                    }

                    try {
                        file = new FileInputStream("/tmp/" + clientRequest.getChunkName());
                        fin = new ObjectInputStream(file);
                        c = (chunk) fin.readObject();
                        fin.close();
                        file.close();
                    } catch (IOException e) {   //CHUNK MISSING
                        try {
                            response = new packetStructure(CHUNK_CORRUPTION, "Chunk corrupted");
                            System.out.println("-- <ERROR> Chunk " + clientRequest.getChunkName() + "missing on disk.");
                            System.out.println("<- <" + clientSocket.getRemoteSocketAddress() + "> Chunk " + clientRequest.getChunkName() + " corrupt.");
                            clientOS.writeObject(response);
                        } catch (IOException e1) {
                            try {
                                System.out.println("-- <ERROR> Client failed or dead.");
                                clientSocket.close();
                                break;
                            } catch (IOException e2) {
                                e2.printStackTrace();
                                break;
                            }
                        }
                        boolean chunkFetched = requestChunk(clientRequest);
                        if (chunkFetched) {
                            System.out.println("-- <INFO> Chunk " + clientRequest.getChunkName() + "fetched successfully.");
                        } else {
                            System.out.println("-- <ERROR> " + clientRequest.getChunkName() + " is not retrievable. Controller will be notified in next heartbeat.");
                            localChunkList.remove(clientRequest.getChunkName());
                        }

                        break;
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                        break;
                    }

                    Vector<Integer> check = c.verifyIntegrity();

                    if (check.size() == 0) {    //SUCCESS
                        try {
                            response = new packetStructure(CHUNK_READ_PACKET, new chunkReadPacket(c.getMetadata(), c.getData()));
                            System.out.println("<- <" + clientSocket.getRemoteSocketAddress() + "> Read Response " + c.getMetadata().getChunkName() + ".");
                            clientOS.writeObject(response);
                        } catch (IOException e) {
                            try {
                                System.out.println("-- <ERROR> Client failed or dead.");
                                clientSocket.close();
                                break;
                            } catch (IOException e1) {
                                e1.printStackTrace();
                                break;
                            }
                        }
                    } else {        //INTEGRITY FAILURE
                        System.out.println("-- <ERROR> Integrity Check failed for " + clientRequest.getChunkName() + ".");
                        try {
                            response = new packetStructure(CHUNK_CORRUPTION, "Chunk corrupted");
                            System.out.println("<- <" + clientSocket.getRemoteSocketAddress() + "> Chunk " + clientRequest.getChunkName() + " corrupt.");
                            clientOS.writeObject(response);
                        } catch (IOException e) {
                            try {
                                System.out.println("-- <ERROR> Client failed or dead.");
                                clientSocket.close();
                                break;
                            } catch (IOException e1) {
                                e1.printStackTrace();
                                break;
                            }
                        }

                        if (processSliceFix(check, clientRequest.getChunkName(), clientRequest.getOtherHosts())) {
                            System.out.println("-- <INFO> Slices for " + clientRequest.getChunkName() + "recovered successfully.");
                        }
                    }
                }
                break;
                case CHUNK_WRITE_PACKET: {
                    boolean failed = false;
                    Socket hostSocket;
                    chunkWritePacket cwp = null;
                    FileOutputStream file = null;
                    ObjectOutputStream fout = null;
                    packetStructure replyPacket = null;
                    ObjectInputStream relayin = null;
                    ObjectOutputStream relayout = null;

                    try {
                        cwp = new chunkWritePacket(packet.getPayloadPacket());
                        System.out.println("-> <" + clientSocket.getRemoteSocketAddress() + "> New Chunk " + cwp.getChunkContents().getMetadata().getChunkName() + ".");
                        c = new chunk(cwp.getChunkContents().getMetadata(), cwp.getChunkContents().getData());

                        System.out.println("Version : " + cwp.getChunkContents().getMetadata().getVersionNo());
                        File f = new File("/tmp/" + c.getMetadata().getChunkName());
                        f.getParentFile().mkdirs();
                        file = new FileOutputStream(f);
                        fout = new ObjectOutputStream(file);
                        fout.writeObject(c);
                        fout.close();
                        file.close();

                        for (Map.Entry<String, chunkMetadata> entry : localChunkList.entrySet()) {
                            if (entry.getKey().equals(c.getMetadata().getChunkName())) {
                                localChunkList.remove(entry.getKey());
                            }
                        }

                        for (Map.Entry<String, chunkMetadata> entry : localChunkListNew.entrySet()) {
                            if (entry.getKey().equals(c.getMetadata().getChunkName())) {
                                localChunkListNew.remove(entry.getKey());
                            }
                        }

                        localChunkListNew.put(c.getMetadata().getChunkName(), c.getMetadata());
                        localChunkList.put(c.getMetadata().getChunkName(), c.getMetadata());

                        if (cwp.getRelayHosts().size() == 0) {
                            System.out.println("-- <INFO> No more hosts to relay.");
                            try {
                                clientOS.writeObject(new packetStructure(CHUNK_RELAY_SUCCESSFUL, new String("Relay successful.")));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            break;
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    for (hostAddress h : cwp.getRelayHosts()) {
                        failed = false;
                        try {
                            hostSocket = new Socket(h.getHostIp(), h.getHostPort());
                        } catch (IOException e) {
                            failed = true;
                            continue;
                        }

                        try {
                            relayout = new ObjectOutputStream(hostSocket.getOutputStream());
                            relayin = new ObjectInputStream(hostSocket.getInputStream());
                        } catch (IOException e) {
                            failed = true;
                            try {
                                hostSocket.close();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            continue;
                        }

                        try {
                            packet = new packetStructure(CHUNK_WRITE_PACKET, new chunkWritePacket(excludeHost(h, cwp.getRelayHosts()), cwp.getChunkContents()));
                            System.out.println("<- <" + hostSocket.getRemoteSocketAddress() + "> Relay " + cwp.getChunkContents().getMetadata().getChunkName() + ".");
                            relayout.writeObject(packet);
                            System.out.println("-> <" + hostSocket.getRemoteSocketAddress() + "> Relay response.");
                            Object o = relayin.readObject();
                            System.out.println("<- <" + hostSocket.getRemoteSocketAddress() + "> Relay response forward.");
                            clientOS.writeObject(o);
                            break;
                        } catch (IOException e) {
                            failed = true;
                            e.printStackTrace();
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                    }

                    if (failed) {
                        System.out.println("-- <ERROR> Failed to relay.");
                        try {
                            clientOS.writeObject(new packetStructure(CHUNK_RELAY_FAILURE, new String("Failed to relay file.")));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
                case CHUNK_REMOVE_REQUEST: {
                    chunkRemoveRequest crr = null;
                    try {
                        crr = new chunkRemoveRequest(packet.getPayloadPacket());
                        System.out.println("-> <" + clientSocket.getRemoteSocketAddress() + "> Chunk Remove Request from Controller for " + crr.getChunkName() + ".");

                        localChunkList.remove(crr.getChunkName());
                        localChunkListNew.remove(crr.getChunkName());

                        File f = new File("/tmp/" + crr.getChunkName());
                        if (f.exists()) {
                            f.delete();
                        }

                        clientOS.writeObject(new packetStructure(CHUNK_REMOVED, "Chunk Removed"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }

                    for (String s : localChunkList.keySet()) {
                        System.out.println(s);
                    }
                }
                break;
                case CHUNK_PATCH_REQUEST: {
                    chunkPatchRequest cr = null;
                    FileInputStream file;
                    ObjectInputStream fin = null;
                    ObjectOutputStream pout = null;
                    ObjectInputStream pin = null;
                    packetStructure response;
                    try {
                        cr = new chunkPatchRequest(packet.getPayloadPacket());
                        System.out.println("-> <" + clientSocket.getRemoteSocketAddress() + "> Chunk Patch Request from Controller " + cr.getChunkName() + ".");
                        //System.out.println(cr.getChunkName());
                        //System.out.println(cr.getHost().getHostIp() + cr.getHost().getHostPort());

                        Socket s = new Socket(cr.getHost().getHostIp(), cr.getHost().getHostPort());


                        file = new FileInputStream("/tmp/" + cr.getChunkName());
                        fin = new ObjectInputStream(file);
                        c = (chunk) fin.readObject();

                        pout = new ObjectOutputStream(s.getOutputStream());
                        pin = new ObjectInputStream(s.getInputStream());
                        System.out.println("<- <" + s.getRemoteSocketAddress() + "> Write" + c.getMetadata().getChunkName());
                        pout.writeObject(new packetStructure(CHUNK_WRITE_PACKET, new chunkWritePacket(new Vector<hostAddress>(), c.getDataPacket())));
                        response = (packetStructure) pin.readObject();

                        if (response.getPacketType() == CHUNK_RELAY_SUCCESSFUL) {
                            System.out.println("-> <" + s.getRemoteSocketAddress() + "> Chunk Patch Successful.");
                            clientOS.writeObject(new packetStructure(CHUNK_PATCH_SUCCESSFUL, "Chunk Patch Successful."));
                        } else {
                            System.out.println("-> <" + s.getRemoteSocketAddress() + "> Chunk Patch Failed.");
                            clientOS.writeObject(new packetStructure(CHUNK_PATCH_FAILED, "Chunk Patch Failed."));
                        }
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
                case CHUNK_OFFLOAD_REQUEST: {
                    chunkOffloadRequest cor = null;
                    FileInputStream file;
                    ObjectInputStream fin = null;
                    ObjectOutputStream pout = null;
                    ObjectInputStream pin = null;
                    packetStructure response;
                    try {
                        cor = new chunkOffloadRequest(packet.getPayloadPacket());
                        System.out.println("-> <" + clientSocket.getRemoteSocketAddress() + "> Chunk Offload Request from Controller.");
                        //System.out.println(cr.getChunkName());
                        //System.out.println(cr.getHost().getHostIp() + cr.getHost().getHostPort());

                        Socket s = new Socket(cor.getHost().getHostIp(), cor.getHost().getHostPort());


                        file = new FileInputStream("/tmp/" + cor.getChunkName());
                        fin = new ObjectInputStream(file);
                        c = (chunk) fin.readObject();
                        fin.close();
                        file.close();

                        pout = new ObjectOutputStream(s.getOutputStream());
                        pin = new ObjectInputStream(s.getInputStream());
                        System.out.println("<- <" + s.getRemoteSocketAddress() + "> Write" + c.getMetadata().getChunkName());
                        pout.writeObject(new packetStructure(CHUNK_WRITE_PACKET, new chunkWritePacket(new Vector<hostAddress>(), c.getDataPacket())));
                        response = (packetStructure) pin.readObject();

                        if (response.getPacketType() == CHUNK_RELAY_SUCCESSFUL) {
                            System.out.println("-> <" + s.getRemoteSocketAddress() + "> Chunk Offload Successful.");
                            clientOS.writeObject(new packetStructure(CHUNK_OFFLOAD_SUCCESS, "Chunk Offload Successful."));
                            localChunkList.remove(cor.getChunkName());
                            File f = new File("/tmp/" + cor.getChunkName());
                            if (f.exists()) {
                                f.delete();
                            }
                        } else {
                            System.out.println("-> <" + s.getRemoteSocketAddress() + "> Chunk Offload Failed.");
                            clientOS.writeObject(new packetStructure(CHUNK_OFFLOAD_FAILED, "Chunk Offload Failed."));
                        }
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
                case CHUNK_SERVER_PING: {
                    packetStructure pingPacket;
                    packetStructure pongPacket;

                    while (true) {
                        try {
                            pongPacket = new packetStructure(CHUNK_SERVER_PONG, "PONG.");
                            System.out.println("<- <" + clientSocket.getRemoteSocketAddress() + "> PONG.");
                            clientOS.writeObject(pongPacket);
                            //System.out.println("PONG");
                            pingPacket = (packetStructure) clientIS.readObject();

                            if (pingPacket.getPacketType() == CHUNK_SERVER_PING) {
                                System.out.println("-> <" + clientSocket.getRemoteSocketAddress() + "> PING.");
                                //System.out.println("PING");
                            } else {
                                System.out.println("-- <ERROR> Controller Failure.");
                                break;
                            }
                        } catch (Exception e) {
                            System.out.println("-- <ERROR> Controller Failure.");
                            break;
                        }
                    }
                }
                break;
                default:
                    break;
            }
        }

        public boolean processSliceFix(Vector<Integer> check, String chunkName, Vector<hostAddress> hosts) {
            boolean success = true;

            Socket chunkServerSocket;

            ObjectOutputStream out;
            ObjectInputStream in;

            packetStructure packet;
            packetStructure reply;
            chunkSliceRequest request;
            chunkSlices data;


            for (hostAddress h : hosts) {

                try {
                    chunkServerSocket = new Socket(h.getHostIp(), h.getHostPort());
                    out = new ObjectOutputStream(chunkServerSocket.getOutputStream());
                    in = new ObjectInputStream(chunkServerSocket.getInputStream());
                    request = new chunkSliceRequest(check, chunkName);
                    packet = new packetStructure(CHUNK_SLICE_READ_REQUEST, request);
                    System.out.print("<- <" + chunkServerSocket.getRemoteSocketAddress() + "> Slice Read " + request.getChunkName() + "{ ");
                    for (Integer i : request.getSliceList()) {
                        System.out.print(i + " ");
                    }
                    System.out.println("}");
                    out.writeObject(packet);
                    reply = (packetStructure) in.readObject();

                    if (reply.getPacketType() == CHUNK_SLICES) {
                        data = new chunkSlices(reply.getPayloadPacket());
                        System.out.print("-> <" + chunkServerSocket.getRemoteSocketAddress() + "> Slice Read Response " + data.getSlices().keySet() + "{");
                        for (Integer i : data.getSlices().keySet()) {
                            System.out.print(i + " ");
                        }
                        System.out.println("}");

                        byte[] currData;
                        ObjectInputStream oin = new ObjectInputStream(new FileInputStream(new File("/tmp" + chunkName)));

                        chunk c = (chunk) oin.readObject();

                        oin.close();
                        currData = c.getData();

                        ByteBuffer buf = ByteBuffer.wrap(currData);
                        buf.mark();

                        for (Map.Entry<Integer, byte[]> entry : data.getSlices().entrySet()) {
                            buf.position((entry.getKey() - 1) * chunk.SLICE_SIZE);
                            buf.put(entry.getValue());
                            buf.reset();
                        }

                        chunk cwrite = new chunk(c.getMetadata(), buf.array());

                        ObjectOutputStream fout = new ObjectOutputStream(new FileOutputStream("/tmp/" + chunkName));
                        fout.writeObject(cwrite);
                        fout.close();
                        success = true;
                    } else {
                        //TODO Case where chunk slice error message packet came in
                        success = false;
                    }
                } catch (IOException e) {
                    success = false;
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    success = false;
                    e.printStackTrace();
                }

                if (success)
                    break;
            }


            return success;
        }

        public Vector<hostAddress> getStrippedHostList(Vector<hostAddress> hosts, Vector<hostAddress> chunkHostMapping) {
            Vector<hostAddress> strippedList = new Vector<>(chunkHostMapping);
            for (Iterator<hostAddress> iterator = strippedList.iterator(); iterator.hasNext(); ) {
                hostAddress r = iterator.next();
                for (hostAddress h : hosts) {
                    if (h.getHostIp().equalsIgnoreCase(r.getHostIp()) && h.getHostPort() == r.getHostPort()) {
                        iterator.remove();
                    }
                }
            }

            return strippedList;
        }

        public Vector<hostAddress> excludeHost(hostAddress h, Vector<hostAddress> chunkHostMapping) {
            Vector<hostAddress> strippedList = new Vector<>(chunkHostMapping);
            for (Iterator<hostAddress> iterator = strippedList.iterator(); iterator.hasNext(); ) {
                hostAddress r = iterator.next();
                if (h.getHostIp().equalsIgnoreCase(r.getHostIp()) && h.getHostPort() == r.getHostPort()) {
                    iterator.remove();
                }
            }

            return strippedList;
        }


        private boolean requestChunk(chunkRequest clientRequest) {
            System.out.print("-- <INFO> Recovery initiated for " + clientRequest.getChunkName());
            boolean success = false;
            packetStructure packet = null;

            if (clientRequest.getOtherHosts().size() == 0)
                return false;
            //response.setChunkHostMapping(response.excludeHost(new hostAddress(chunkServerIP.firstElement(), chunkServerPort)));

            Vector<hostAddress> exclude = new Vector<>();
            for (hostAddress h : clientRequest.getOtherHosts()) {
                exclude.add(h);
                success = false;
                try {
                    packet = new packetStructure(CHUNK_READ_REQUEST, new chunkRequest(clientRequest.getChunkName(), getStrippedHostList(exclude, clientRequest.getOtherHosts())));
                    Socket chunkServerSocket = new Socket(h.getHostIp(), h.getHostPort());
                    ObjectOutputStream chunkServerSocketOS = new ObjectOutputStream(chunkServerSocket.getOutputStream());
                    ObjectInputStream chunkServerSocketIS = new ObjectInputStream(chunkServerSocket.getInputStream());

                    System.out.println("<- <" + chunkServerSocket.getRemoteSocketAddress() + "> Read " + clientRequest.getChunkName() + ".");
                    chunkServerSocketOS.writeObject(packet);
                    packet = (packetStructure) chunkServerSocketIS.readObject();

                    if (packet.getPacketType() == CHUNK_READ_PACKET) {
                        chunkReadPacket crp = new chunkReadPacket(packet.getPayloadPacket());
                        System.out.println("-> <" + chunkServerSocket.getRemoteSocketAddress() + "> Read Response " + crp.getMetadata().getChunkName() + ".");
                        chunk c = new chunk(crp.getMetadata(), crp.getData());

                        File f = new File("/tmp/" + c.getMetadata().getChunkName());
                        f.getParentFile().mkdirs();
                        FileOutputStream file = new FileOutputStream(f);
                        ObjectOutputStream fout = new ObjectOutputStream(file);
                        fout.writeObject(c);
                        fout.close();
                        file.close();
                        success = true;
                        break;
                    } else if (packet.getPacketType() == CHUNK_CORRUPTION) {
                        System.out.println("-> <" + chunkServerSocket.getRemoteSocketAddress() + "> Chunk " + clientRequest.getChunkName() + " corrupt.");
                        success = false;
                    }
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    success = false;
                } catch (IOException e) {
                    System.out.println("-- <ERROR> Disk or Network I/O failed.");
                    success = false;
                }
            }

            return success;
        }
    }
}
