package controllerRequests;

import chunk.chunkMetadata;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Vector;

/**
 * Created by iyro on 9/22/15.
 */
public class chunkWriteSuccess implements Serializable {
    private chunkMetadata meta;
    private Vector<String> chunksToBeRemoved;

    public chunkWriteSuccess(chunkMetadata meta, Vector<String> chunksToBeRemoved) {
        this.meta = meta;
        this.chunksToBeRemoved = chunksToBeRemoved;
    }

    public chunkWriteSuccess(byte[] b) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(b);
        ObjectInputStream is = new ObjectInputStream(in);

        chunkWriteSuccess f = (chunkWriteSuccess) is.readObject();

        this.meta = f.getMeta();
        this.chunksToBeRemoved = f.getChunksToBeRemoved();

        is.close();
        in.close();
    }

    public chunkMetadata getMeta() {
        return meta;
    }

    public Vector<String> getChunksToBeRemoved() {
        return chunksToBeRemoved;
    }
}
