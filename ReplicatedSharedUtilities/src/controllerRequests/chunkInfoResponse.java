package controllerRequests;

import chunk.*;
import chunkServerInfo.hostAddress;
import packets.packetStructure;

import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import static packets.packetStructure.*;

/**
 * Created by iyro on 9/1/15.
 */

public class chunkInfoResponse implements java.io.Serializable {
    public static final int BLOCK_SIZE = 64000;
    private int fileOperation;
    private String chunkName;
    private chunkMetadata metadata = null;
    private Vector<hostAddress> chunkHostMapping = null;

    public chunkInfoResponse(byte[] b) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(b);
        ObjectInputStream is = new ObjectInputStream(in);

        chunkInfoResponse f = (chunkInfoResponse) is.readObject();

        this.chunkName = f.getChunkName();
        this.fileOperation = f.getFileOperation();
        this.metadata = f.getMetadata();
        this.chunkHostMapping = f.getChunkHostMappings();

        is.close();
        in.close();
    }

    public void setChunkHostMapping(Vector<hostAddress> chunkHostMapping) {
        this.chunkHostMapping = chunkHostMapping;
    }

    public chunkInfoResponse(chunkMetadata metadata, int fileOperation, Vector<hostAddress> chunkHostMapping) {
        this.chunkName = metadata.getChunkName();
        this.fileOperation = fileOperation;
        this.chunkHostMapping = chunkHostMapping;
        this.metadata = metadata;
    }

    public String getChunkName() {
        return chunkName;
    }

    public int getFileOperation() {
        return fileOperation;
    }

    public chunkMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(chunkMetadata metadata) {
        this.metadata = metadata;
    }

    public Vector<hostAddress> getChunkHostMappings() {
        return chunkHostMapping;
    }

    public boolean processReadResponse() {
        boolean success = true;

        Socket chunkServerSocket;
        ObjectOutputStream out;
        ObjectInputStream in;

        packetStructure packet;
        packetStructure reply;
        chunkRequest request;
        chunkReadPacket data;

        Vector<hostAddress> exclude = new Vector<>();
        for (hostAddress h : chunkHostMapping) {
            success = true;
            exclude.add(h);
            try {
                chunkServerSocket = new Socket(h.getHostIp(), h.getHostPort());
                out = new ObjectOutputStream(chunkServerSocket.getOutputStream());
                in = new ObjectInputStream(chunkServerSocket.getInputStream());
                request = new chunkRequest(chunkName, getStrippedHostList(exclude));

                packet = new packetStructure(CHUNK_READ_REQUEST, request);
                System.out.println("<- <" + chunkServerSocket.getRemoteSocketAddress() + "> Read chunk" + request.getChunkName() + ".");
                out.writeObject(packet);
                reply = (packetStructure) in.readObject();

                if (reply.getPacketType() == CHUNK_READ_PACKET) {
                    data = new chunkReadPacket(reply.getPayloadPacket());
                    System.out.println("-> <" + chunkServerSocket.getRemoteSocketAddress() + "> Chunk " + data.getMetadata().getChunkName() + ".");
                    metadata = data.getMetadata();
                    try {
                        File file = new File("/tmp/" + chunkName);
                        file.getParentFile().mkdirs();
                        Path path = Paths.get("/tmp/" + chunkName);
                        Files.write(path, data.getData());
                        success = true;
                    } catch (IOException e) {
                        System.out.println("-- <ERROR> Failed to write chunk to disk.");
                        success = false;
                    }
                } else if (reply.getPacketType() == CHUNK_CORRUPTION) {
                    System.out.println("-- <INFO> Chunk corrupt at " + h.getHostIp() + ":" + h.getHostPort() + ".");
                    success = false;
                    continue;
                }
            } catch (IOException e) {
                System.out.println("-- <ERROR> Communication Failure.");
                success = false;
                continue;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                success = false;
                continue;
            }

            if (success)
                break;

        }

        return success;
    }

    public boolean processWriteResponse() {
        boolean success;

        Socket chunkServerSocket;

        ObjectOutputStream out = null;
        ObjectInputStream in = null;

        packetStructure packet;
        packetStructure reply;
        chunkReadPacket data = null;

        success = true;
        Path path = Paths.get("/tmp/" + chunkName);
        byte[] input = null;

        try {
            input = Files.readAllBytes(path);
        } catch (IOException e1) {
            System.out.println("-- <ERROR> Chunk not found on disk.");
            return false;
        }

        for (hostAddress h : chunkHostMapping) {
            success = true;

            try {
                chunkServerSocket = new Socket(h.getHostIp(), h.getHostPort());

                out = new ObjectOutputStream(chunkServerSocket.getOutputStream());
                in = new ObjectInputStream(chunkServerSocket.getInputStream());

                chunkReadPacket cr = new chunkReadPacket(metadata, input);
                chunkWritePacket chunk = new chunkWritePacket(excludeHost(h), cr);

                packet = new packetStructure(CHUNK_WRITE_PACKET, chunk);
                System.out.println("<- <" + chunkServerSocket.getRemoteSocketAddress() + "> Chunk" + cr.getMetadata().getChunkName() + ".");
                out.writeObject(packet);
                reply = (packetStructure) in.readObject();

                if (reply.getPacketType() == CHUNK_RELAY_SUCCESSFUL) {
                    System.out.println("-> <" + chunkServerSocket.getRemoteSocketAddress() + "> Relay successful." + cr.getMetadata().getChunkName() + ".");
                    success = true;
                    break;
                } else {
                    System.out.println("<- <" + chunkServerSocket.getRemoteSocketAddress() + "> Relay failure." + cr.getMetadata().getChunkName() + ".");
                    success = false;
                }

            } catch (IOException e) {
                System.out.println("-- <ERROR> Communication Failure.");
                success = false;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return success;
    }

    public Vector<hostAddress> excludeHost(hostAddress h) {
        Vector<hostAddress> strippedList = new Vector<>(chunkHostMapping);

        for (Iterator<hostAddress> iterator = strippedList.iterator(); iterator.hasNext(); ) {
            hostAddress r = iterator.next();
            if (h.getHostIp().equalsIgnoreCase(r.getHostIp()) && h.getHostPort() == r.getHostPort()) {
                iterator.remove();
            }
        }

        return strippedList;
    }

    public Vector<hostAddress> getStrippedHostList(Vector<hostAddress> hosts) {
        Vector<hostAddress> strippedList = new Vector<>(chunkHostMapping);

        for (Iterator<hostAddress> iterator = strippedList.iterator(); iterator.hasNext(); ) {
            hostAddress r = iterator.next();
            for (hostAddress h : hosts) {
                if (h.getHostIp().equalsIgnoreCase(r.getHostIp()) && h.getHostPort() == r.getHostPort()) {
                    iterator.remove();
                }
            }
        }

        return strippedList;
    }
}