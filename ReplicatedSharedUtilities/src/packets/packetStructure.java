package packets;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Created by iyro on 9/3/15.
 */
public class packetStructure implements java.io.Serializable {
    public static final int CHUNK_INFO_REQUEST = 1;
    public static final int CHUNK_INFO_RESPONSE = 2;
    public static final int CHUNK_NOT_FOUND = 8;
    public static final int CHUNK_READ_REQUEST = 3;
    public static final int CHUNK_WRITE_PACKET = 4;
    public static final int CHUNK_SERVER_MINOR_HEARTBEAT = 5;
    public static final int CHUNK_SERVER_MAJOR_HEARTBEAT = 6;
    public static final int CHUNK_RELAY_FAILURE = 10;
    public static final int CHUNK_RELAY_SUCCESSFUL = 9;
    public static final int CHUNK_PATCH_REQUEST = 11;
    public static final int CHUNK_PATCH_SUCCESSFUL = 12;
    public static final int CHUNK_PATCH_FAILED = 13;
    public static final int CHUNK_SERVER_PING = 14;
    public static final int CHUNK_SERVER_PONG = 15;
    public static final int CHUNK_SLICE_READ_REQUEST = 16;
    public static final int CHUNK_SLICES = 17;
    public static final int CHUNK_READ_PACKET = 18;
    public static final int CHUNK_CORRUPTION = 19;
    public static final int CHUNK_OFFLOAD_REQUEST = 20;
    public static final int CHUNK_OFFLOAD_SUCCESS = 21;
    public static final int CHUNK_OFFLOAD_FAILED = 22;
    public static final int CHUNK_REMOVE_REQUEST = 23;
    public static final int CHUNK_REMOVED = 24;
    public static final int CHUNK_RELAY_REMOVE_EXTRAS = 25;
    public static final int CONTROLLER_HEARTBEAT = 7;

    private int packetType;

    private byte[] payloadPacket = null;

    public packetStructure(int type, Object f) throws IOException {
        this.packetType = type;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(out);
        os.writeObject(f);
        payloadPacket = out.toByteArray();
        os.close();
        out.close();
    }

    public int getPacketType() {
        return packetType;
    }

    public void setPacketType(int packetType) {
        this.packetType = packetType;
    }

    public byte[] getPayloadPacket() {
        return payloadPacket;
    }

    public void setPayloadPacket(byte[] payloadPacket) {
        this.payloadPacket = payloadPacket;
    }
}
