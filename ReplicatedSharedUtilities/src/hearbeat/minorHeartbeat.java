/**
 * Created by iyro on 8/29/15.
 */
package hearbeat;

import chunk.chunkMetadata;
import chunkServerInfo.hostAddress;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.concurrent.ConcurrentHashMap;

public class minorHeartbeat implements java.io.Serializable {
    private hostAddress address;
    private long remainingSpace;
    private int totalChunks;
    private ConcurrentHashMap<String, chunkMetadata> chunkList;

    public minorHeartbeat(hostAddress address, long remainingSpace, ConcurrentHashMap<String, chunkMetadata> chunkList, int totalChunks) {
        this.address = address;
        this.remainingSpace = remainingSpace;
        this.chunkList = chunkList;
        this.totalChunks = totalChunks;
    }

    public minorHeartbeat(byte[] b) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(b);
        ObjectInputStream is = new ObjectInputStream(in);
        minorHeartbeat f = (minorHeartbeat) is.readObject();

        this.address = f.getAddress();
        this.remainingSpace = f.getRemainingSpace();
        this.chunkList = f.getChunkList();
        this.totalChunks = f.getTotalChunks();
    }

    public hostAddress getAddress() {
        return address;
    }

    public long getRemainingSpace() {
        return remainingSpace;
    }

    public int getTotalChunks() {
        return totalChunks;
    }

    public ConcurrentHashMap<String, chunkMetadata> getChunkList() {
        return chunkList;
    }
}

