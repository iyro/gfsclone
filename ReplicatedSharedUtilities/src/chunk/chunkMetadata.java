package chunk;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by iyro on 9/5/15.
 */
public class chunkMetadata implements Serializable {
    private String chunkName;
    private int sequenceNum;
    private int totalChunks;
    private int versionNo;
    private Date timestamp;

    public chunkMetadata(String chunkName, int sequenceNum, int versionNo, Date timestamp, int totChunks) {
        this.chunkName = chunkName;
        this.sequenceNum = sequenceNum;
        this.versionNo = versionNo;
        this.timestamp = timestamp;
        this.totalChunks = totChunks;
    }

    public chunkMetadata(chunkMetadata metadata) {
        this.chunkName = metadata.getChunkName();
        this.sequenceNum = metadata.getSequenceNum();
        this.versionNo = metadata.getVersionNo();
        this.timestamp = metadata.getTimestamp();
        this.totalChunks = metadata.getTotalChunks();
    }

    public chunkMetadata(byte[] b) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(b);
        ObjectInputStream is = new ObjectInputStream(in);

        chunkMetadata f = (chunkMetadata) is.readObject();

        this.chunkName = f.getChunkName();
        this.sequenceNum = f.getSequenceNum();
        this.versionNo = f.getVersionNo();
        this.timestamp = f.getTimestamp();
        this.totalChunks = f.getTotalChunks();

        is.close();
        in.close();
    }

    public void setSequenceNum(int sequenceNum) {
        this.sequenceNum = sequenceNum;
    }

    public void setTotalChunks(int totalChunks) {
        this.totalChunks = totalChunks;
    }

    public void setVersionNo(int versionNo) {
        this.versionNo = versionNo;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public int getTotalChunks() {
        return totalChunks;
    }

    public String getChunkName() {
        return chunkName;
    }

    public void setChunkName(String chunkName) {
        this.chunkName = chunkName;
    }

    public int getSequenceNum() {
        return sequenceNum;
    }

    public int getVersionNo() {
        return versionNo;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
