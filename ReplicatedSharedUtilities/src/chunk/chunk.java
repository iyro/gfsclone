package chunk;

/**
 * Created by iyro on 9/5/15.
 */

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.Vector;

public class chunk implements Serializable {
    public static final int SLICE_SIZE = 8000;
    public static final int CHUNK_SIZE_ON_DISK = 65000;
    private chunkReadPacket cPacket;
    private Vector<byte[]> integrityInfo = new Vector<>();

    public chunk(chunkMetadata metadata, byte[] cData) {
        this.cPacket = new chunkReadPacket(metadata, cData);

        try {
            int offset = 0;
            while (offset < this.getData().length) {
                MessageDigest sha1 = MessageDigest.getInstance("SHA1");
                int len = SLICE_SIZE;
                if (this.getData().length - offset < SLICE_SIZE) {
                    len = this.getData().length - offset;
                }
                sha1.update(this.getData(), offset, len);
                integrityInfo.add(sha1.digest());

                offset += len;
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public chunk(byte[] b) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(b);
        ObjectInputStream is = new ObjectInputStream(in);

        chunk f = (chunk) is.readObject();

        this.cPacket = f.getDataPacket();
        this.integrityInfo = f.getIntegrityInfo();

        is.close();
        in.close();
    }

    private static String byteArray2Hex(byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        return formatter.toString();
    }

    public chunkMetadata getMetadata() {
        return cPacket.getMetadata();
    }

    public byte[] getData() {
        return cPacket.getData();
    }

    public chunkReadPacket getDataPacket() {
        return cPacket;
    }

    public Vector<byte[]> getIntegrityInfo() {
        return integrityInfo;
    }

    public Vector<Integer> verifyIntegrity() {
        Vector<Integer> chunksToBeFixed = new Vector<>();
        try {
            int offset = 0;
            int index = 0;
            for (byte[] b : this.integrityInfo) {
                MessageDigest sha1 = MessageDigest.getInstance("SHA1");
                int len = SLICE_SIZE;
                if (this.getData().length - offset < SLICE_SIZE) {
                    len = this.getData().length - offset;
                }
                sha1.update(this.getData(), offset, len);
                offset += len;

                index++;
                if (!MessageDigest.isEqual(sha1.digest(), b)) {
                    chunksToBeFixed.add(index);
                }
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return chunksToBeFixed;
    }
}