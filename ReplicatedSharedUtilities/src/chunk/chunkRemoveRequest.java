package chunk;

import chunkServerInfo.hostAddress;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

/**
 * Created by iyro on 9/22/15.
 */
public class chunkRemoveRequest implements Serializable {
    private String chunkName;

    public chunkRemoveRequest(String chunkName) {
        this.chunkName = chunkName;
    }

    public chunkRemoveRequest(byte[] b) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(b);
        ObjectInputStream is = new ObjectInputStream(in);
        chunkRemoveRequest f = (chunkRemoveRequest) is.readObject();
        this.chunkName = f.getChunkName();
    }

    public String getChunkName() {
        return chunkName;
    }
}
