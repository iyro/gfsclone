package chunk;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

/**
 * Created by iyro on 9/9/15.
 */
public class chunkReadPacket implements Serializable {
    protected chunkMetadata metadata;
    private byte[] data;

    public chunkReadPacket(byte[] b) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(b);
        ObjectInputStream is = new ObjectInputStream(in);

        chunkReadPacket f = (chunkReadPacket) is.readObject();

        this.metadata = new chunkMetadata(f.getMetadata());
        this.data = f.getData();

        is.close();
        in.close();
    }

    public chunkReadPacket(chunkMetadata metadata, byte[] data) {
        this.metadata = metadata;
        this.data = data;
    }

    public chunkMetadata getMetadata() {
        return metadata;
    }

    public byte[] getData() {
        return data;
    }
}
