package controllerRequests;

import ErasureCodedClient.ErasureCodedFileOperations;
import chunkServerInfo.hostAddress;
import packets.packetStructure;
import chunk.*;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Vector;

import static packets.packetStructure.*;

/**
 * Created by iyro on 9/1/15.
 */

public class chunkInfoResponse implements Serializable {
    public static final int BLOCK_SIZE = 64000;
    private int fileOperation;
    private String chunkName;
    private chunkMetadata metadata = null;
    private Vector<hostAddress> chunkHostMapping = null;

    public chunkInfoResponse(byte[] b) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(b);
        ObjectInputStream is = new ObjectInputStream(in);

        chunkInfoResponse f = (chunkInfoResponse) is.readObject();

        this.chunkName = f.getChunkName();
        this.fileOperation = f.getFileOperation();
        this.metadata = f.getMetadata();
        this.chunkHostMapping = f.getChunkHostMappings();

        is.close();
        in.close();
    }

    public void setChunkHostMapping(Vector<hostAddress> chunkHostMapping) {
        this.chunkHostMapping = chunkHostMapping;
    }

    public chunkInfoResponse(chunkMetadata metadata, int fileOperation, Vector<hostAddress> chunkHostMapping) {
        this.chunkName = metadata.getChunkName();
        this.fileOperation = fileOperation;
        this.chunkHostMapping = chunkHostMapping;
        this.metadata = metadata;
    }

    public String getChunkName() {
        return chunkName;
    }

    public int getFileOperation() {
        return fileOperation;
    }

    public chunkMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(chunkMetadata metadata) {
        this.metadata = metadata;
    }

    public Vector<hostAddress> getChunkHostMappings() {
        return chunkHostMapping;
    }

    public boolean processReadResponse(String notShardName) {

        ErasureCodedFileOperations reeds;
        byte[][] shards = new byte[ErasureCodedFileOperations.TOTAL_SHARDS][];
        boolean[] shardPresent = new boolean[ErasureCodedFileOperations.TOTAL_SHARDS];
        int shardCount = 0;

        int i = 0;
        for (hostAddress h : chunkHostMapping) {
            Socket chunkServerSocket;

            ObjectOutputStream out;
            ObjectInputStream in;

            packetStructure packet;
            packetStructure reply;
            chunkRequest request;
            chunkReadPacket data;

            i++;
            try {
                chunkServerSocket = new Socket(h.getHostIp(), h.getHostPort());
                out = new ObjectOutputStream(chunkServerSocket.getOutputStream());
                in = new ObjectInputStream(chunkServerSocket.getInputStream());
                request = new chunkRequest(notShardName + "_shard" + i, new Vector<>());
                packet = new packetStructure(CHUNK_READ_REQUEST, request);
                System.out.println("<- <" + chunkServerSocket.getRemoteSocketAddress() + "> Read Shard " + request.getChunkName() + "_shard" + i + ".");
                out.writeObject(packet);
                reply = (packetStructure) in.readObject();

                if (reply.getPacketType() == CHUNK_READ_PACKET) {
                    data = new chunkReadPacket(reply.getPayloadPacket());
                    metadata = data.getMetadata();
                    shards[i - 1] = data.getData();
                    shardPresent[i - 1] = true;
                    shardCount++;
                } else if (reply.getPacketType() == CHUNK_CORRUPTION) {
                    System.out.println("Shard corrupt at " + h.getHostIp() + ":" + h.getHostPort() + ".");
                    shards[i] = null;
                    shardPresent[i] = false;
                    continue;
                }
            } catch (IOException e) {
                shards[i] = null;
                shardPresent[i] = false;
            } catch (ClassNotFoundException e) {
                shards[i] = null;
                shardPresent[i] = false;
            }
        }

        if (shardCount >= ErasureCodedFileOperations.DATA_SHARDS) {
            try {
                reeds = new ErasureCodedFileOperations(notShardName, shards, shardPresent, shardCount);
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        } else {
            System.out.println("Chunk not retrievable.");
            return false;
        }
    }

    public boolean processWriteResponse(String notShardName) {
        Path path = Paths.get("/tmp/" + notShardName);
        byte[] input = null;
        ErasureCodedFileOperations reeds;
        byte[][] shards;
        boolean success = false;

        try {
            input = Files.readAllBytes(path);
            reeds = new ErasureCodedFileOperations(notShardName, input);
            shards = reeds.getShards();
        } catch (IOException e1) {
            System.out.println("Chunk not found on disk.");
            return false;
        }

        int i = 0;
        for (hostAddress h : chunkHostMapping) {
            i++;
            Socket chunkServerSocket;

            ObjectOutputStream out = null;
            ObjectInputStream in = null;

            packetStructure packet;
            packetStructure reply;
            chunkReadPacket data = null;

            success = true;

            try {
                chunkServerSocket = new Socket(h.getHostIp(), h.getHostPort());

                out = new ObjectOutputStream(chunkServerSocket.getOutputStream());
                in = new ObjectInputStream(chunkServerSocket.getInputStream());

                metadata.setChunkName(notShardName + "_shard" + i);
                chunkReadPacket cr = new chunkReadPacket(metadata, shards[i - 1]);
                chunkWritePacket chunk = new chunkWritePacket(new Vector<hostAddress>(), cr);

                packet = new packetStructure(CHUNK_WRITE_PACKET, chunk);
                out.writeObject(packet);
                reply = (packetStructure) in.readObject();

                if (reply.getPacketType() == CHUNK_RELAY_SUCCESSFUL) {
                    success = true;
                    continue;
                } else
                    success = false;

            } catch (IOException e) {
                success = false;
                break;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return success;
    }
}