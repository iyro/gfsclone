package controllerRequests;

/**
 * Created by iyro on 9/1/15.
 */

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class chunkInfoRequest implements java.io.Serializable {
    /*
     *1 - Read
     *2 - Write
     *3 - Update
     */
    public static final int INVALID_OPERATION = 0;
    public static final int FILE_READ = 1;
    public static final int FILE_WRITE = 2;
    public static final int FILE_UPDATE = 3;

    public int fileOperation;
    public String chunkName;
    public long totalChunks = 0;

    public chunkInfoRequest(int fileOperation, String chunkName, long totalChunks) {
        this.fileOperation = fileOperation;
        this.chunkName = chunkName;
        this.totalChunks = totalChunks;
    }

    public chunkInfoRequest(int fileOperation, String chunkName) {
        this.fileOperation = fileOperation;
        this.chunkName = chunkName;
    }

    public chunkInfoRequest(byte[] b) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(b);
        ObjectInputStream is = new ObjectInputStream(in);
        chunkInfoRequest f = (chunkInfoRequest) is.readObject();
        this.chunkName = f.getChunkName();
        this.fileOperation = f.getFileOperation();
        this.totalChunks = f.getTotalChunks();
    }

    public String getChunkName() {
        return chunkName;
    }

    public int getFileOperation() {
        return fileOperation;
    }

    public long getTotalChunks() {
        return totalChunks;
    }
}
