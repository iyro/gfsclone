/**
 * Created by iyro on 8/29/15.
 */
package chunk;

import chunkServerInfo.hostAddress;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Vector;

public class chunkRequest implements Serializable {
    private String chunkName;
    private Vector<Integer> slices = new Vector<>();
    private Vector<hostAddress> otherHosts = null;

    public chunkRequest(byte[] b) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(b);
        ObjectInputStream is = new ObjectInputStream(in);
        chunkRequest f = (chunkRequest) is.readObject();
        this.chunkName = f.getChunkName();
        this.slices = f.getSlices();
        this.otherHosts = f.getOtherHosts();
    }

    public chunkRequest(String chunkName, Vector<hostAddress> otherHosts) {
        this.chunkName = chunkName;
        this.otherHosts = otherHosts;
    }

    public chunkRequest(String chunkName, Vector<hostAddress> otherHosts, Vector<Integer> slices) {
        this.chunkName = chunkName;
        this.otherHosts = otherHosts;
        this.slices = slices;
    }

    public String getChunkName() {
        return chunkName;
    }

    public void setChunkName(String chunkName) {
        this.chunkName = chunkName;
    }

    public Vector<Integer> getSlices() {
        return slices;
    }

    public Vector<hostAddress> getOtherHosts() {
        return otherHosts;
    }
}
