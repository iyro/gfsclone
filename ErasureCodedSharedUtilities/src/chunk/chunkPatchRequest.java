package chunk;

import chunkServerInfo.hostAddress;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

/**
 * Created by iyro on 9/18/15.
 */
public class chunkPatchRequest implements Serializable {
    private hostAddress host;
    private String chunkName;

    public chunkPatchRequest(hostAddress h, String chunkName) {
        this.host = h;
        this.chunkName = chunkName;
    }

    public chunkPatchRequest(byte[] b) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(b);
        ObjectInputStream is = new ObjectInputStream(in);
        chunkPatchRequest f = (chunkPatchRequest) is.readObject();
        this.chunkName = f.getChunkName();
        this.host = f.getHost();
    }

    public hostAddress getHost() {
        return host;
    }

    public String getChunkName() {
        return chunkName;
    }
}
