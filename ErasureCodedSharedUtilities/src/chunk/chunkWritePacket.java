package chunk;

import chunkServerInfo.hostAddress;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Vector;

/**
 * Created by iyro on 9/10/15.
 */
public class chunkWritePacket implements Serializable {
    Vector<hostAddress> relayHosts;
    chunkReadPacket chunkContents;

    public chunkWritePacket(Vector<hostAddress> relayHosts, chunkReadPacket chunkContents) {
        this.relayHosts = relayHosts;
        this.chunkContents = chunkContents;
    }

    public chunkWritePacket(byte[] b) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(b);
        ObjectInputStream is = new ObjectInputStream(in);
        chunkWritePacket f = (chunkWritePacket) is.readObject();
        this.relayHosts = f.getRelayHosts();
        this.chunkContents = f.getChunkContents();
        is.close();
        in.close();
    }

    public Vector<hostAddress> getRelayHosts() {
        return relayHosts;
    }

    public chunkReadPacket getChunkContents() {
        return chunkContents;
    }
}
