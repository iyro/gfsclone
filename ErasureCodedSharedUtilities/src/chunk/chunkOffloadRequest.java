package chunk;

import chunkServerInfo.hostAddress;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

/**
 * Created by iyro on 9/22/15.
 */
public class chunkOffloadRequest implements Serializable {
    private hostAddress host;
    private String chunkName;

    public chunkOffloadRequest(hostAddress host, String chunkName) {
        this.host = host;
        this.chunkName = chunkName;
    }

    public chunkOffloadRequest(byte[] b) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(b);
        ObjectInputStream is = new ObjectInputStream(in);
        chunkOffloadRequest f = (chunkOffloadRequest) is.readObject();
        this.chunkName = f.getChunkName();
        this.host = f.getHost();
    }

    public hostAddress getHost() {
        return host;
    }

    public String getChunkName() {
        return chunkName;
    }
}
