package chunk;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by iyro on 9/19/15.
 */
public class chunkSlices {
    private HashMap<Integer, byte[]> slices = new HashMap<>();

    public chunkSlices(HashMap<Integer, byte[]> slices) {
        this.slices = slices;
    }

    public chunkSlices(byte[] b) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(b);
        ObjectInputStream is = new ObjectInputStream(in);
        chunkSlices f = (chunkSlices) is.readObject();
        this.slices = f.getSlices();
    }

    public HashMap<Integer, byte[]> getSlices() {
        return slices;
    }

    public void setSlices(Map.Entry<Integer, byte[]> slice) {
        this.slices.put(slice.getKey(), slice.getValue());
    }

    public void setSlices(Integer i, byte[] b) {
        this.slices.put(i, b);
    }
}
