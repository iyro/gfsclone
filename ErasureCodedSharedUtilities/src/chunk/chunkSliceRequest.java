package chunk;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Vector;

/**
 * Created by iyro on 9/19/15.
 */
public class chunkSliceRequest {
    private String chunkName;
    private Vector<Integer> sliceList;

    public chunkSliceRequest(Vector<Integer> sliceList, String chunkName) {
        this.sliceList = sliceList;
        this.chunkName = chunkName;
    }

    public chunkSliceRequest(byte[] b) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(b);
        ObjectInputStream is = new ObjectInputStream(in);
        chunkRequest f = (chunkRequest) is.readObject();
        this.chunkName = f.getChunkName();
        this.sliceList = f.getSlices();
    }

    public String getChunkName() {
        return chunkName;
    }

    public Vector<Integer> getSliceList() {
        return sliceList;
    }
}
