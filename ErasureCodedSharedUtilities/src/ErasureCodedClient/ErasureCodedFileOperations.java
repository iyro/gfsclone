package ErasureCodedClient;

import erasure.ReedSolomon;

import java.io.*;
import java.nio.ByteBuffer;

/**
 * Created by iyro on 9/20/15.
 */
public class ErasureCodedFileOperations {
    public static final int DATA_SHARDS = 6;
    public static final int PARITY_SHARDS = 3;
    public static final int TOTAL_SHARDS = 9;
    public static final int BYTES_IN_INT = 4;

    byte[][] shards;
    String chunkName;

    long fileSize;

    public ErasureCodedFileOperations(String chunkName, byte[] allBytes) throws IOException {
        ReedSolomon reedSolomon = new ReedSolomon(DATA_SHARDS, PARITY_SHARDS);
        this.chunkName = chunkName;

        this.fileSize = new File("/tmp/" + chunkName).length();

        int storedSize = (int) fileSize + BYTES_IN_INT;

        int shardSize = (storedSize + DATA_SHARDS - 1) / DATA_SHARDS;

        int bufferSize = shardSize * DATA_SHARDS;
        //byte[] allBytes = new byte[bufferSize];

        ByteArrayOutputStream bou = new ByteArrayOutputStream(bufferSize);

        DataOutputStream dou = new DataOutputStream(bou);

        dou.writeInt((int) fileSize);

        dou.write(allBytes);

        if (dou.size() < bufferSize) {
            while (dou.size() != bufferSize) {
                dou.write('\0');
            }
        }

        shards = new byte[TOTAL_SHARDS][shardSize];

        for (int i = 0; i < DATA_SHARDS; i++) {
            System.arraycopy(bou.toByteArray(), i * shardSize, shards[i], 0, shardSize);
        }

        reedSolomon.encodeParity(shards, 0, shardSize);
    }

    public ErasureCodedFileOperations(String s, byte[][] shards, boolean[] shardPresent, int shardCount) throws IOException {
        ReedSolomon reedSolomon = new ReedSolomon(DATA_SHARDS, PARITY_SHARDS);
        int shardSize = 0;

        this.chunkName = s;
        if (shardCount < DATA_SHARDS) {
            System.out.println("Insufficient number of shards.");
            return;
        }

        boolean first = true;
        for (int i = 0; i < TOTAL_SHARDS; i++) {
            if (shardPresent[i] && first) {
                shardSize = shards[i].length;
                first = false;
                continue;
            }

            if (shardPresent[i] && shardSize != shards[i].length) {
                System.out.println("Shard size mismatch.");
                return;
            }
        }

        for (int i = 0; i < TOTAL_SHARDS; i++) {
            if (!shardPresent[i]) {
                shards[i] = new byte[shardSize];
            }
        }

        reedSolomon.decodeMissing(shards, shardPresent, 0, shardSize);

        byte[] allBytes = new byte[shardSize * DATA_SHARDS];

        ByteBuffer target = ByteBuffer.wrap(allBytes);

        for (int i = 0; i < DATA_SHARDS; i++) {
            target.put(shards[i]);
        }

        ByteArrayInputStream in = new ByteArrayInputStream(allBytes);
        DataInputStream din = new DataInputStream(in);

        fileSize = din.readInt();

        FileOutputStream f = new FileOutputStream("/tmp/" + chunkName);
        f.write(allBytes, 4, (int) fileSize);
        f.close();
        System.out.println("chunk " + chunkName + " created.");
    }

    public byte[][] getShards() {
        return shards;
    }
}
