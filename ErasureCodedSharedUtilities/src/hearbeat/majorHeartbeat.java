package hearbeat;

import chunk.chunkMetadata;
import chunkServerInfo.hostAddress;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by iyro on 9/5/15.
 */
public class majorHeartbeat implements java.io.Serializable {
    private hostAddress address;
    private long remainingSpace;
    private int totalChunks;
    private ConcurrentHashMap<String, chunkMetadata> chunkList;

    public majorHeartbeat(hostAddress address, long remainingSpace, ConcurrentHashMap<String, chunkMetadata> chunkList, int totalChunks) {
        this.address = address;
        this.remainingSpace = remainingSpace;
        this.chunkList = chunkList;
        this.totalChunks = totalChunks;
    }

    public majorHeartbeat(byte[] b) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(b);
        ObjectInputStream is = new ObjectInputStream(in);
        majorHeartbeat f = (majorHeartbeat) is.readObject();

        this.address = f.getAddress();
        this.remainingSpace = f.getRemainingSpace();
        this.chunkList = f.getChunkList();
        this.totalChunks = f.getTotalChunks();
    }

    public hostAddress getAddress() {
        return address;
    }

    public long getRemainingSpace() {
        return remainingSpace;
    }

    public int getTotalChunks() {
        return totalChunks;
    }

    public void setTotalChunks(int totalChunks) {
        this.totalChunks = totalChunks;
    }

    public ConcurrentHashMap<String, chunkMetadata> getChunkList() {
        return chunkList;
    }
}
