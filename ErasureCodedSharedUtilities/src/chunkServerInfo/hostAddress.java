package chunkServerInfo;

import java.io.Serializable;

/**
 * Created by iyro on 9/5/15.
 */
public class hostAddress implements Serializable {

    private String hostIp;
    private int hostPort;

    public hostAddress(String hostIp, int hostPort) {
        this.hostIp = hostIp;
        this.hostPort = hostPort;
    }

    public String getHostIp() {
        return hostIp;
    }

    public int getHostPort() {
        return hostPort;
    }

}
