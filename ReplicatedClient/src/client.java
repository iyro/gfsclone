/**
 * Created by iyro on 8/29/15.
 */

import chunk.chunkMetadata;
import controllerRequests.chunkInfoRequest;
import controllerRequests.chunkInfoResponse;
import controllerRequests.chunkWriteSuccess;
import packets.packetStructure;

import java.io.*;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Vector;

import static controllerRequests.chunkInfoRequest.*;
import static packets.packetStructure.*;

public class client {
    private static final int BLOCK_SIZE = 64000;
    static String controllerIP = "localhost";
    static int controllerPort = 0;
    static Socket controllerSocket = null;
    static String fileName;
    static int fileOperation;

    public static void main(String args[]) {
        ObjectOutputStream controllerOS = null;
        ObjectInputStream controllerIS = null;

        if (args.length != 4) {
            System.out.println("-- <FATAL ERROR> Malformed arguments.");
            System.out.print("Usage : java -jar ReplicatedClient <read|write> <Absolute File Path> <Controller IP> <Controller Port>");
            System.exit(1);
        }

        if (checkIPv4(args[2]))
            controllerIP = args[2];
        else {
            System.out.println("-- <FATAL ERROR> Invalid IP string " + args[2] + ".");
            System.out.print("Usage : java -jar ReplicatedClient <read|write> <Absolute File Path> <Controller IP> <Controller Port>");
            System.exit(1);
        }

        try {
            controllerPort = Integer.parseInt(args[3]);
            if (controllerPort < 1024 || controllerPort > 65535) {
                throw new Exception();
            }
        } catch (Exception e) {
            System.out.println("-- <FATAL ERROR> Invalid Port " + args[1] + ".");
            System.out.print("Usage : java -jar ReplicatedClient <read|write> <Absolute File Path> <Controller IP> <Controller Port>");
            System.exit(1);
        }

        setParameters(args[0], args[1]);

        try {
            controllerSocket = new Socket(controllerIP, controllerPort);
            controllerOS = new ObjectOutputStream(controllerSocket.getOutputStream());
            controllerIS = new ObjectInputStream(controllerSocket.getInputStream());
            System.out.println("-- <INFO> Connected to controller (" + controllerSocket.getRemoteSocketAddress() + ").");
        } catch (IOException e1) {
            System.out.println("-- <FATAL ERROR> Cannot connect to controller at " + controllerIP + ':' + controllerPort);
            System.exit(1);
        }

        long totalChunks = 0;
        int chunkNumber;
        boolean success = true;
        switch (fileOperation) {
            case FILE_READ:
                chunkMetadata meta = null;
                for (chunkNumber = 1; chunkNumber <= totalChunks || totalChunks == 0; chunkNumber++) {
                    chunkInfoRequest request = new chunkInfoRequest(fileOperation, fileName + "_chunk" + chunkNumber);
                    packetStructure packet = null;

                    try {
                        packet = new packetStructure(CHUNK_INFO_REQUEST, request);
                        System.out.println("<- <" + controllerSocket.getRemoteSocketAddress() + "> Read " + request.getChunkName());
                        controllerOS.writeObject(packet);
                    } catch (IOException e1) {
                        System.out.println("-- <ERROR> Could not create packet.");
                        success = false;
                        break;
                    }

                    try {
                        packet = (packetStructure) controllerIS.readObject();
                    } catch (IOException e1) {
                        System.out.println("-- <ERROR> Controller may be down.");
                        success = false;
                        break;
                    } catch (ClassNotFoundException e1) {
                        success = false;
                        break;
                    }

                    if (packet.getPacketType() == CHUNK_INFO_RESPONSE) {
                        chunkInfoResponse response = null;
                        try {
                            response = new chunkInfoResponse(packet.getPayloadPacket());
                            System.out.println("-> <" + controllerSocket.getRemoteSocketAddress() + "> Read response" + response.getChunkName());
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                            success = false;
                            break;
                        } catch (IOException e) {
                            success = false;
                            break;
                        }

                        if (response.processReadResponse()) {
                            if (meta == null || meta.getVersionNo() == response.getMetadata().getVersionNo()) {
                                meta = response.getMetadata();
                            }
                            totalChunks = response.getMetadata().getTotalChunks();
                        } else {
                            System.out.println("-- <ERROR> Chunk could not be fetched." + response.getChunkName());
                            success = false;
                            break;
                        }
                    } else if (packet.getPacketType() == CHUNK_NOT_FOUND) {
                        System.out.println("-> <" + controllerSocket.getRemoteSocketAddress() + "> Chunk not found." + request.getChunkName());
                        success = false;
                        break;
                    }
                }

                if (success) {
                    success = reassembleFile(fileName, (int) totalChunks);
                    if (success) {
                        System.out.println("-- <INFO> " + fileName + " created.");
                        System.out.println("-- <INFO> Version : " + meta.getVersionNo());
                        System.out.println("-- <INFO> Timestamp : " + meta.getTimestamp());
                    }
                }
                else
                    System.out.println("-- <ERROR> Something went wrong.");

                fileCleanup(fileName);
                break;
            case FILE_WRITE:
                totalChunks = disassembleFile(fileName);

                if (totalChunks == 0) {
                    System.exit(1);
                }

                chunkMetadata metaData = new chunkMetadata("", 0, 0, new Date(), (int) totalChunks);
                for (chunkNumber = 1; chunkNumber <= totalChunks; chunkNumber++) {
                    chunkInfoRequest request = new chunkInfoRequest(fileOperation, fileName + "_chunk" + chunkNumber, totalChunks);

                    packetStructure packet = null;
                    int existingTotalChunks = 0;
                    try {
                        packet = new packetStructure(CHUNK_INFO_REQUEST, request);
                        System.out.println("<- <" + controllerSocket.getRemoteSocketAddress() + "> Write " + request.getChunkName());
                        controllerOS.writeObject(packet);
                    } catch (IOException e1) {
                        System.out.println("-- <ERROR> Could not create packet.");
                        success = false;
                        break;
                    }

                    try {
                        packet = (packetStructure) controllerIS.readObject();

                        if (packet.getPacketType() == CHUNK_INFO_RESPONSE) {
                            chunkInfoResponse response = null;
                            try {
                                response = new chunkInfoResponse(packet.getPayloadPacket());
                                System.out.println("-> <" + controllerSocket.getRemoteSocketAddress() + "> Write response" + response.getChunkName());
                                metaData.setChunkName(response.getChunkName());
                                metaData.setSequenceNum(chunkNumber);
                                metaData.setVersionNo(response.getMetadata().getVersionNo() + 1);
                                existingTotalChunks = response.getMetadata().getTotalChunks();
                                response.setMetadata(metaData);
                            } catch (ClassNotFoundException e) {
                                e.printStackTrace();
                                success = false;
                                break;
                            } catch (IOException e) {
                                success = false;
                                break;
                            }

                            if (!response.processWriteResponse()) {
                                System.out.println("-- <ERROR> Could not write. " + response.getChunkName());
                                success = false;
                                break;
                            } else {
                                if (chunkNumber != totalChunks) {
                                    try {
                                        System.out.println("<- <" + controllerSocket.getRemoteSocketAddress() + "> Write successful." + response.getChunkName());
                                        controllerOS.writeObject(new packetStructure(CHUNK_RELAY_SUCCESSFUL, response.getMetadata()));
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    Vector<String> chunksToBeRemoved = new Vector<>();

                                    for (long i = totalChunks + 1; i < existingTotalChunks; i++) {
                                        chunksToBeRemoved.add(fileName + "_chunk" + i);
                                    }
                                    try {
                                        System.out.println("<- <" + controllerSocket.getRemoteSocketAddress() + "> Write successful." + response.getChunkName());
                                        controllerOS.writeObject(new packetStructure(CHUNK_RELAY_REMOVE_EXTRAS, new chunkWriteSuccess(response.getMetadata(), chunksToBeRemoved)));
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    } catch (IOException e1) {
                        System.out.println("-- <ERROR> Controller may be down.");
                        success = false;
                        break;
                    } catch (ClassNotFoundException e1) {
                        System.out.println("-- <ERROR> Controller may be down.");
                        success = false;
                        break;
                    }
                }

                if (success) {
                    System.out.println("-- <INFO> " + fileName + " written.");
                    System.out.println("-- <INFO> Version : " + metaData.getVersionNo());
                    System.out.println("-- <INFO> Timestamp : " + metaData.getTimestamp());
                }
                fileCleanup(fileName);
                break;
            default:
                System.out.println("-- <ERROR> Invalid option.");
                break;
        }
    }

    public static final boolean checkIPv4(final String ip) {
        boolean isIPv4;
        try {
            final InetAddress inet = InetAddress.getByName(ip);
            isIPv4 = inet.getHostAddress().equals(ip)
                    && inet instanceof Inet4Address;
        } catch (final UnknownHostException e) {
            isIPv4 = false;
        }
        return isIPv4;
    }

    private static void fileCleanup(String fileName) {
        System.out.println("-- <INFO> Cleaning up tmp files.");
        String chunkName = "/tmp/" + fileName;
        File dir = new File(chunkName);

        if (dir.getParentFile().isDirectory()) {
            for (File f : dir.getParentFile().listFiles()) {
                if (f.getName().startsWith(dir.getName())) {
                    f.delete();
                }
            }
        }
    }

    private static void setParameters(String fop, String name) {
        if (fop.equalsIgnoreCase("read")) {
            fileOperation = FILE_READ;
        } else if (fop.equalsIgnoreCase("write")) {
            fileOperation = FILE_WRITE;
        } else {
            fileOperation = INVALID_OPERATION;
            System.out.print("-- <FATAL ERROR> Invalid Operation" + fop);
            System.out.print("Usage : java -jar ReplicatedClient <read|write> <Absolute File Path> <Controller IP> <Controller Port>");
            System.exit(1);
        }

        File f = new File(name);
        if ((fileOperation == FILE_WRITE && f.exists()) || fileOperation == FILE_READ)
            fileName = name;
        else {
            System.out.print("-- <FATAL ERROR> File " + fileName + " does not exist.");
            System.out.print("Usage : java -jar ReplicatedClient <read|write> <Absolute File Path> <Controller IP> <Controller Port>");
            System.exit(1);
        }
    }

    public static boolean reassembleFile(String fileName, int totChunks) {
        System.out.println("-- <INFO> Assembling Chunks.");
        int chunkNum = 1;
        File f = new File(fileName);

        FileInputStream infile = null;

        byte[] buffer = new byte[BLOCK_SIZE];

        if (f.exists() && !f.isDirectory()) {
            f.delete();
        } else {
            f.getParentFile().mkdirs();
        }

        try {
            FileOutputStream outfile = new FileOutputStream(f);
            for (chunkNum = 1; chunkNum <= totChunks; chunkNum++) {

                String chunkName = "/tmp/" + fileName + "_chunk" + chunkNum;

                infile = new FileInputStream(chunkName);
                int b = 0;
                while ((b = infile.read(buffer)) >= 0) {
                    outfile.write(buffer, 0, b);
                    outfile.flush();
                }

            }
            return true;
        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException e) {
            return false;
        }
    }

    public static long disassembleFile(String fileName) {
        System.out.println("-- <INFO> Splitting into chunks.");
        int chunkNum = 1;
        long totalChunks = 0;

        File f = new File(fileName);

        if (f.exists() && !f.isDirectory()) {
            try {
                FileInputStream infile = new FileInputStream(f);

                if (infile.getChannel().size() % BLOCK_SIZE == 0)
                    totalChunks = infile.getChannel().size() / BLOCK_SIZE;
                else
                    totalChunks = (infile.getChannel().size() / BLOCK_SIZE) + 1;

                while (chunkNum <= totalChunks) {
                    File file = new File("/tmp/" + fileName + "_chunk" + chunkNum);
                    file.getParentFile().mkdirs();
                    FileOutputStream outfile = new FileOutputStream(file);

                    byte[] buffer = new byte[BLOCK_SIZE];
                    int b = 0;

                    b = infile.read(buffer);
                    outfile.write(buffer, 0, b);
                    outfile.close();

                    chunkNum++;
                }
            } catch (IOException e) {
                totalChunks = 0;
                e.printStackTrace();
            }
        } else {
            System.out.println("File does not exist or is a directory.");
        }
        return totalChunks;
    }
}