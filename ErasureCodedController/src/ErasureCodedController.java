/**
 * Created by iyro on 8/29/15.
 */

import static chunk.chunk.CHUNK_SIZE_ON_DISK;

import ErasureCodedClient.ErasureCodedFileOperations;
import chunk.chunkMetadata;
import chunk.chunkOffloadRequest;
import chunk.chunkPatchRequest;
import chunk.chunkRemoveRequest;
import chunkServerInfo.hostAddress;
import controllerRequests.chunkInfoRequest;
import controllerRequests.chunkInfoResponse;
import controllerRequests.chunkWriteSuccess;
import hearbeat.majorHeartbeat;
import hearbeat.minorHeartbeat;
import packets.packetStructure;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static controllerRequests.chunkInfoRequest.*;
import static packets.packetStructure.*;

public class ErasureCodedController {
    protected static int controllerServerPort = 43210;
    protected static ServerSocket controllerServerSocket = null;
    private static volatile Vector<hostAddress> chunkServers = new Vector<>();
    private static volatile ConcurrentHashMap<hostAddress, ConcurrentHashMap<String, chunkMetadata>> controllerChunkMappings = new ConcurrentHashMap<>();
    private static volatile ConcurrentHashMap<hostAddress, Long> freeSpaceMappings = new ConcurrentHashMap<>();
    private static volatile ConcurrentHashMap<hostAddress, Integer> totalChunkMappings = new ConcurrentHashMap<>();

    protected static void printMappings() {
        System.out.println("################################################################################");
        for (Map.Entry<hostAddress, ConcurrentHashMap<String, chunkMetadata>> hosts : controllerChunkMappings.entrySet()) {
            System.out.println(hosts.getKey().getHostIp() + ":" + hosts.getKey().getHostPort());
            for (Map.Entry<String, chunkMetadata> files : hosts.getValue().entrySet()) {
                System.out.println("\t\tName : " + files.getValue().getChunkName());
                System.out.println("\t\tSequence : " + files.getValue().getSequenceNum());
                System.out.println("\t\tVersion : " + files.getValue().getVersionNo());
                System.out.println("\t\tTimestamp : " + files.getValue().getTimestamp());
            }
        }
        System.out.println("################################################################################");
    }

    public static void main(String[] args) {

        if (args.length != 0 && args.length != 1) {
            System.out.println("-- <FATAL ERROR> Invalid number of arguments. Required 0 or 1.");
            System.exit(1);
        }

        if (args.length == 1) {
            controllerServerPort = Integer.parseInt(args[0]);
        }

        try {
            controllerServerSocket = new ServerSocket(controllerServerPort);
        } catch (IOException e) {
            throw new RuntimeException("Cannot open port " + controllerServerPort, e);
        }

        System.setProperty("java.net.preferIPv4Stack", "true");
        Enumeration e = null;
        try {
            e = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e1) {
            e1.printStackTrace();
        }
        while (e.hasMoreElements()) {
            NetworkInterface n = (NetworkInterface) e.nextElement();
            Enumeration ee = n.getInetAddresses();
            while (ee.hasMoreElements()) {
                InetAddress i = (InetAddress) ee.nextElement();
                if (i instanceof Inet6Address || i.isLoopbackAddress() || i.isSiteLocalAddress())
                    continue;
                System.out.println("-- <INFO> Controller listening on " + i.getHostAddress() + ":" + controllerServerSocket.getLocalPort());
            }
        }

        while (true) {
            Socket clientSocket = null;
            try {
                clientSocket = controllerServerSocket.accept();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            new Thread(
                    new controllerWorkerThread(
                            clientSocket)
            ).start();
        }
    }

    public static class controllerPingPongThread implements Runnable {
        private hostAddress h;
        private Socket s;

        public controllerPingPongThread(hostAddress h) {
            this.h = h;
            try {
                this.s = new Socket(h.getHostIp(), h.getHostPort());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        private void initiatePatch() {
            System.out.println("-- <INFO> Initiate Patching.");
            HashMap<String, chunkMetadata> chunksToBePatched = null;

            chunksToBePatched = new HashMap<>(controllerChunkMappings.get(h));
            chunkServers.remove(h);
            controllerChunkMappings.remove(h);
            freeSpaceMappings.remove(h);
            totalChunkMappings.remove(h);

            for (Map.Entry<String, chunkMetadata> patch : chunksToBePatched.entrySet()) {
                Vector<hostAddress> chunkMapping = new Vector<>();

                hostAddress hostToBePatched = null;

                for (Map.Entry<hostAddress, ConcurrentHashMap<String, chunkMetadata>> hosts : controllerChunkMappings.entrySet()) {
                    for (Map.Entry<String, chunkMetadata> chunks : hosts.getValue().entrySet()) {
                        if (patch.getKey().equals(chunks.getKey())) {
                            chunkMapping.add(hosts.getKey());
                        }
                    }
                }

                int existingChunks = 0;
                while (hostToBePatched == null) {
                    for (Map.Entry<hostAddress, Integer> entry : totalChunkMappings.entrySet()) {
                        if (chunkMapping.contains(entry.getKey())) {
                            continue;
                        }
                        if (entry.getValue() == existingChunks) {
                            hostToBePatched = entry.getKey();
                            break;
                        }
                    }
                    existingChunks++;
                }

                packetStructure request = null;
                try {
                    request = new packetStructure(CHUNK_PATCH_REQUEST, new chunkPatchRequest(hostToBePatched, patch.getKey()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                packetStructure response = null;

                for (hostAddress h : chunkMapping) {
                    try {
                        Socket s = new Socket(h.getHostIp(), h.getHostPort());

                        ObjectOutputStream outs = new ObjectOutputStream(s.getOutputStream());
                        ObjectInputStream ins = new ObjectInputStream(s.getInputStream());
                        System.out.println("<- <" + s.getRemoteSocketAddress() + "> Replicate " + patch.getKey() + " (" + h.getHostIp() + ":" + h.getHostPort() + "->" + hostToBePatched.getHostIp() + ":" + hostToBePatched.getHostPort() + ")");
                        outs.writeObject(request);

                        response = (packetStructure) ins.readObject();
                        if (response.getPacketType() == CHUNK_PATCH_SUCCESSFUL) {
                            System.out.println("-> <" + s.getRemoteSocketAddress() + "> Replicated " + patch.getKey() + " (" + h.getHostIp() + ":" + h.getHostPort() + "->" + hostToBePatched.getHostIp() + ":" + hostToBePatched.getHostPort() + ")");
                            break;
                        }
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        @Override
        public void run() {
            ObjectInputStream in;
            ObjectOutputStream out;
            packetStructure packet;
            packetStructure response;

            try {
                out = new ObjectOutputStream(s.getOutputStream());
                in = new ObjectInputStream(s.getInputStream());

                while (true) {
                    packet = new packetStructure(CHUNK_SERVER_PING, "PING.");
                    System.out.println("-> <" + s.getRemoteSocketAddress() + "> PING");
                    out.writeObject(packet);
                    response = (packetStructure) in.readObject();

                    if (response.getPacketType() == CHUNK_SERVER_PONG) {
                        System.out.println("<- <" + s.getRemoteSocketAddress() + "> PONG");
                        Thread.sleep(30000);
                    } else {
                        System.out.println("-- <ERROR> Chunk server Failure.");
                        initiatePatch();
                        System.out.println("-- <INFO> Pruned " + h.getHostIp() + ":" + h.getHostPort());
                        break;
                    }
                }

            } catch (Exception e) {
                System.out.println("-- <ERROR> Chunk server Failure.");
                initiatePatch();
                System.out.println("-- <INFO> Pruned " + h.getHostIp() + ":" + h.getHostPort());
            }
        }
    }

    private static class controllerWorkerThread implements Runnable {

        //private static final int BLOCK_SIZE = 64000;
        private static final int CHUNK_SERVER = 1;
        private static final int CLIENT = 2;

        private Socket clientSocket = null;
        private hostAddress currHost = null;
        private int clientType;

        public controllerWorkerThread(Socket clientSocket) {
            this.clientSocket = clientSocket;
        }

        protected void printHeartbeat(majorHeartbeat h) {
            System.out.println("################################################################################");
            System.out.println("Received Major Heartbeat from " + clientSocket.getRemoteSocketAddress());
            System.out.println("\tChunk Server IP : " + h.getAddress().getHostIp());
            System.out.println("\tChunk Server Port : " + h.getAddress().getHostPort());
            System.out.println("\tFree Space : " + h.getRemainingSpace());
            System.out.println("\tTotal Chunks : " + h.getTotalChunks());
            for (Map.Entry<String, chunkMetadata> entry : h.getChunkList().entrySet()) {
                System.out.println("\tChunk Name : " + entry.getKey());
                System.out.println("\t\tSequence Number : " + entry.getValue().getSequenceNum());
                System.out.println("\t\tVersion Number : " + entry.getValue().getVersionNo());
                System.out.println("\t\tTimestamp : " + entry.getValue().getTimestamp());
            }
            System.out.println("################################################################################");
        }

        protected void printHeartbeat(minorHeartbeat h) {
            System.out.println("################################################################################");
            System.out.println("Received Minor Heartbeat from " + clientSocket.getRemoteSocketAddress());
            System.out.println("\tChunk Server IP : " + h.getAddress().getHostIp());
            System.out.println("\tChunk Server Port : " + h.getAddress().getHostPort());
            System.out.println("\tFree Space : " + h.getRemainingSpace());
            System.out.println("\tTotal Chunks : " + h.getTotalChunks());
            for (Map.Entry<String, chunkMetadata> entry : h.getChunkList().entrySet()) {
                System.out.println("\tChunk Name : " + entry.getKey());
                System.out.println("\t\tSequence Number : " + entry.getValue().getSequenceNum());
                System.out.println("\t\tVersion Number : " + entry.getValue().getVersionNo());
                System.out.println("\t\tTimestamp : " + entry.getValue().getTimestamp());
            }
            System.out.println("################################################################################");
        }

        public void run() {
            packetStructure packet = null;
            packetStructure reply = null;
            ObjectInputStream in = null;
            ObjectOutputStream out = null;

            try {
                in = new ObjectInputStream(clientSocket.getInputStream());
                out = new ObjectOutputStream(clientSocket.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }

            while (!clientSocket.isClosed()) {
                try {
                    packet = (packetStructure) in.readObject();
                } catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    try {
                        clientSocket.close();
                        //System.out.println("Socket Closed.");
                    } catch (IOException e) {
                        //e.printStackTrace();
                        System.out.println("-- <ERROR> Couldn't close socket " + clientSocket.getRemoteSocketAddress());
                    }
                }

                switch (packet.getPacketType()) {
                    case CHUNK_INFO_REQUEST: {
                        clientType = CLIENT;
                        chunkInfoRequest clientRequest = null;
                        try {
                            clientRequest = new chunkInfoRequest(packet.getPayloadPacket());
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        Vector<hostAddress> chunkMapping = new Vector<>();

                        chunkMetadata m = null;
                        boolean found = false;

                        switch (clientRequest.getFileOperation()) {
                            case FILE_READ: {
                                System.out.println("-> <" + clientSocket.getRemoteSocketAddress() + "> " + "Read " + clientRequest.getChunkName() + ".");
                                int i = 1;
                                while (i <= ErasureCodedFileOperations.TOTAL_SHARDS) {
                                    String shardName = clientRequest.getChunkName() + "_shard" + i;
                                    boolean shardFound = false;
                                    for (Map.Entry<hostAddress, ConcurrentHashMap<String, chunkMetadata>> hosts : controllerChunkMappings.entrySet()) {
                                        for (Map.Entry<String, chunkMetadata> chunks : hosts.getValue().entrySet()) {
                                            if (shardName.equals(chunks.getKey())) {
                                                chunkMapping.add(hosts.getKey());
                                                m = chunks.getValue();
                                                System.out.println("Version No. : " + m.getVersionNo());
                                                shardFound = true;
                                                break;
                                            }
                                        }
                                        if (shardFound)
                                            break;
                                    }

                                    if (!shardFound) {
                                        chunkMapping.add(null);
                                    }
                                    i++;
                                }

                                boolean chunkFound = false;
                                int shardsFound = 0;

                                for (hostAddress h : chunkMapping) {
                                    if (h != null) {
                                        shardsFound++;
                                    }
                                }

                                if (shardsFound >= ErasureCodedFileOperations.DATA_SHARDS) {
                                    try {
                                        packet = new packetStructure(CHUNK_INFO_RESPONSE, new chunkInfoResponse(m, clientRequest.getFileOperation(), chunkMapping));
                                        System.out.println("<- <" + clientSocket.getRemoteSocketAddress() + "> " + "Read Response " + clientRequest.getChunkName() + ".");
                                        out.writeObject(packet);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    System.out.println("-- <INFO> Chunk " + clientRequest.getChunkName() + " not found or not enough shards.");
                                    try {
                                        packet = new packetStructure(CHUNK_NOT_FOUND, new String("Chunk not found."));
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                                //System.out.println("Response for file read request for " + clientRequest.getChunkName() + " sent to " + clientSocket.getRemoteSocketAddress());
                            }
                            break;
                            case FILE_WRITE: {
                                System.out.println("-> <" + clientSocket.getRemoteSocketAddress() + "> " + "Write " + clientRequest.getChunkName() + ".");
                                int operation = clientRequest.getFileOperation();
                                m = new chunkMetadata(clientRequest.getChunkName(), 0, 0, new Date(), 0);
                                int i = 1;

                                HashMap<hostAddress, Integer> totalChunkMappingsCopy = new HashMap<>(totalChunkMappings);
                                while (i <= ErasureCodedFileOperations.TOTAL_SHARDS && chunkMapping.size() < chunkServers.size()) {
                                    String shardName = clientRequest.getChunkName() + "_shard" + i;
                                    boolean shardFound = false;
                                    for (Map.Entry<hostAddress, ConcurrentHashMap<String, chunkMetadata>> hosts : controllerChunkMappings.entrySet()) {
                                        for (Map.Entry<String, chunkMetadata> chunks : hosts.getValue().entrySet()) {
                                            if (shardName.equals(chunks.getKey())) {
                                                chunkMapping.add(hosts.getKey());
                                                m = chunks.getValue();
                                                System.out.println("Version No. : " + m.getVersionNo());
                                                operation = chunkInfoRequest.FILE_UPDATE;
                                                shardFound = true;
                                                break;
                                            }
                                        }
                                        if (shardFound)
                                            break;
                                    }

                                    System.out.println(shardFound);
                                    if (!shardFound) {
                                        Map.Entry<hostAddress, Integer> min = null;
                                        for (Map.Entry<hostAddress, Integer> entry : totalChunkMappingsCopy.entrySet()) {
                                            if (min == null || entry.getValue() < min.getValue()) {
                                                min = entry;
                                            }
                                        }

                                        chunkMapping.add(min.getKey());
                                        totalChunkMappingsCopy.put(min.getKey(), totalChunkMappingsCopy.get(min.getKey()) + 1);
                                    }
                                    i++;
                                }

                                int k = 0;
                                while (chunkMapping.size() < ErasureCodedFileOperations.TOTAL_SHARDS) {
                                    chunkMapping.add(chunkMapping.elementAt(k));
                                    k++;
                                }

                                for (hostAddress h : chunkMapping) {
                                    System.out.println(h.getHostIp() + ":" + h.getHostPort());
                                }

                                if (operation == chunkInfoRequest.FILE_UPDATE) {
                                    System.out.println("-- <INFO> Chunk already exists in the system. Write -> Update.");
                                }

                                try {
                                    packet = new packetStructure(CHUNK_INFO_RESPONSE, new chunkInfoResponse(m, operation, chunkMapping));
                                    out.writeObject(packet);
                                    System.out.println("<- <" + clientSocket.getRemoteSocketAddress() + "> " + "Write Response " + clientRequest.getChunkName() + ".");
                                    //System.out.println("Response for file read request for " + clientRequest.getChunkName() + " sent to " + clientSocket.getRemoteSocketAddress());
                                    reply = (packetStructure) in.readObject();
                                    if (reply.getPacketType() == CHUNK_RELAY_SUCCESSFUL) {
                                        chunkMetadata meta = new chunkMetadata(reply.getPayloadPacket());

                                        int shardNum = 1;
                                        for (hostAddress h : chunkMapping) {
                                            controllerChunkMappings.get(h).put(meta.getChunkName() + "_shard" + shardNum, meta);
                                            totalChunkMappings.put(h, totalChunkMappings.get(h) + 1);
                                            shardNum++;
                                        }

                                        System.out.println("-> <" + clientSocket.getRemoteSocketAddress() + "> " + "Chunk Write successful (" + clientRequest.getChunkName() + ")");
                                    } else if (reply.getPacketType() == CHUNK_RELAY_REMOVE_EXTRAS) {
                                        chunkWriteSuccess cws = new chunkWriteSuccess(reply.getPayloadPacket());
                                        chunkMetadata meta = cws.getMeta();

                                        int shardNum = 1;
                                        for (hostAddress h : chunkMapping) {
                                            controllerChunkMappings.get(h).put(meta.getChunkName() + "_shard" + shardNum, meta);
                                            totalChunkMappings.put(h, totalChunkMappings.get(h) + 1);
                                            shardNum++;
                                        }

                                        System.out.println("-> <" + clientSocket.getRemoteSocketAddress() + "> " + "Chunk Write successful (" + clientRequest.getChunkName() + ")");
                                        for (String s1 : cws.getChunksToBeRemoved()) {
                                            for (int j = 1; j < ErasureCodedFileOperations.TOTAL_SHARDS; j++) {
                                                String s = s1 + "_shard" + j;
                                                for (Map.Entry<hostAddress, ConcurrentHashMap<String, chunkMetadata>> hosts : controllerChunkMappings.entrySet()) {
                                                    for (Map.Entry<String, chunkMetadata> chunks : hosts.getValue().entrySet()) {
                                                        if (s.equals(chunks.getKey())) {
                                                            Socket chunkServerSocket = new Socket(hosts.getKey().getHostIp(), hosts.getKey().getHostPort());

                                                            ObjectOutputStream chunkServerSocketOS = new ObjectOutputStream(chunkServerSocket.getOutputStream());

                                                            ObjectInputStream chunkServerSocketIS = new ObjectInputStream(chunkServerSocket.getInputStream());

                                                            chunkServerSocketOS.writeObject(new packetStructure(CHUNK_REMOVE_REQUEST, new chunkRemoveRequest(s)));

                                                            packetStructure p = (packetStructure) chunkServerSocketIS.readObject();

                                                            if (p.getPacketType() == CHUNK_REMOVED) {
                                                                controllerChunkMappings.get(hosts.getKey()).remove(chunks.getKey());
                                                                totalChunkMappings.put(hosts.getKey(), totalChunkMappings.get(hosts.getKey()) - 1);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (ClassNotFoundException e) {
                                    e.printStackTrace();
                                }
                            }
                            break;
                            default:
                                //TODO
                                break;
                        }
                    }
                    break;
                    case CHUNK_SERVER_MINOR_HEARTBEAT: {
                        clientType = CHUNK_SERVER;
                        minorHeartbeat minor = null;

                        try {
                            minor = new minorHeartbeat(packet.getPayloadPacket());
                            System.out.println("-> <" + clientSocket.getRemoteSocketAddress() + "> " + "Minor Heartbeat. " + minor.getTotalChunks() + ".");
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }

                        for (hostAddress hosts : chunkServers) {
                            if (hosts.getHostPort() == minor.getAddress().getHostPort() &&
                                    hosts.getHostIp().equals(minor.getAddress().getHostIp())) {
                                currHost = hosts;
                                break;
                            }
                        }

                        if (currHost != null) {
                            freeSpaceMappings.remove(currHost);
                            freeSpaceMappings.put(currHost, minor.getRemainingSpace());
                            totalChunkMappings.remove(currHost);
                            totalChunkMappings.put(currHost, minor.getTotalChunks());

                            for (Map.Entry<String, chunkMetadata> chunks : minor.getChunkList().entrySet()) {
                                if (controllerChunkMappings.get(currHost).containsKey(chunks.getKey())) {

                                    controllerChunkMappings.get(currHost).remove(chunks.getKey());

                                    //TODO Check for updated versions in chunk metadata
                                }
                                controllerChunkMappings.get(currHost).put(chunks.getKey(), chunks.getValue());
                            }
                        } else {
                            //TODO handle this case when host is not found for minor heartbeat. Which means it is a new host.
                        }

                        //printHeartbeat(minor);

                        //printMappings();
                    }
                    break;
                    case CHUNK_SERVER_MAJOR_HEARTBEAT: {
                        System.out.println("-> <" + clientSocket.getRemoteSocketAddress() + "> " + "Major Heartbeat. ");
                        clientType = CHUNK_SERVER;
                        majorHeartbeat major = null;

                        try {
                            major = new majorHeartbeat(packet.getPayloadPacket());
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }

                        for (hostAddress hosts : chunkServers) {
                            if (hosts.getHostPort() == major.getAddress().getHostPort() &&
                                    hosts.getHostIp().equals(major.getAddress().getHostIp())) {
                                currHost = hosts;
                                break;
                            }
                        }

                        if (currHost != null) {
                            freeSpaceMappings.remove(currHost);
                            totalChunkMappings.remove(currHost);
                            controllerChunkMappings.remove(currHost);

                            freeSpaceMappings.put(currHost, major.getRemainingSpace());
                            totalChunkMappings.put(currHost, major.getTotalChunks());
                            controllerChunkMappings.put(currHost, major.getChunkList());
                        } else {
                            chunkServers.add(major.getAddress());
                            currHost = major.getAddress();
                            freeSpaceMappings.put(major.getAddress(), major.getRemainingSpace());
                            totalChunkMappings.put(major.getAddress(), major.getTotalChunks());
                            controllerChunkMappings.put(major.getAddress(), major.getChunkList());
                            System.out.println("-- <INFO> " + "New Chunk Server joined. " + currHost.getHostIp() + ":" + currHost.getHostPort());
                            new Thread(
                                    new controllerPingPongThread(
                                            currHost)
                            ).start();

                            normalizeLoad();
                        }

                        //System.out.println(thisHost.getHostIp());
                        //printHeartbeat(major);
                        //printMappings();
                    }
                    break;
                    default:

                        break;
                }
            }
        }

        private void normalizeLoad() {

            int totalChunksInSystem = 0;
            for (Integer i : totalChunkMappings.values())
                totalChunksInSystem += i;

            Vector<String> offloadedChunks = new Vector<>();
            for (int i = 0; i < (totalChunksInSystem / totalChunkMappings.size()); i++) {
                int maxLoad = 0;
                hostAddress loadedHost = null;
                for (Map.Entry<hostAddress, Integer> entry : totalChunkMappings.entrySet()) {
                    if (entry.getValue() > maxLoad) {
                        maxLoad = entry.getValue();
                        loadedHost = entry.getKey();
                    }
                }

                if (maxLoad == 0) {
                    System.out.println("Chunktobeoffloaded null");
                    break;
                }

                if (controllerChunkMappings.get(loadedHost).size() != 0) {
                    String chunkToBeOffloaded = null;

                    for (String e : controllerChunkMappings.get(loadedHost).keySet()) {
                        if (!offloadedChunks.contains(e)) {
                            chunkToBeOffloaded = e;
                            break;
                        }
                    }

                    if (chunkToBeOffloaded == null) {
                        System.out.println("Chunktobeoffloaded null");
                        break;
                    }

                    try {
                        packetStructure packet = new packetStructure(CHUNK_OFFLOAD_REQUEST, new chunkOffloadRequest(currHost, chunkToBeOffloaded));
                        Socket s = new Socket(loadedHost.getHostIp(), loadedHost.getHostPort());

                        ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
                        ObjectInputStream in = new ObjectInputStream(s.getInputStream());

                        System.out.println("<- <" + s.getRemoteSocketAddress() + "> Offload chunk " + chunkToBeOffloaded + ".");
                        out.writeObject(packet);

                        packet = (packetStructure) in.readObject();

                        if (packet.getPacketType() == CHUNK_OFFLOAD_SUCCESS) {
                            System.out.println("-> <" + s.getRemoteSocketAddress() + "> Chunk offload success.");
                            controllerChunkMappings.get(currHost).put(chunkToBeOffloaded, controllerChunkMappings.get(loadedHost).get(chunkToBeOffloaded));
                            controllerChunkMappings.get(loadedHost).remove(chunkToBeOffloaded);
                            totalChunkMappings.put(loadedHost, totalChunkMappings.get(loadedHost) - 1);
                            totalChunkMappings.put(currHost, totalChunkMappings.get(currHost) + 1);
                            offloadedChunks.add(chunkToBeOffloaded);
                        } else {
                            System.out.println("-- <ERROR> Something went wrong with offload request.");
                        }
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    } catch (ClassNotFoundException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }
    }
}